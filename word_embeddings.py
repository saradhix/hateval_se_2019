from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tokenize import TweetTokenizer
import fasttext
import os
import pandas as pd
import numpy as np
import libinfersent as infersent
#import libglove as glove
#import libglovetwitter as glove
#import libfasttext as fasttext
#import fasttext
import gensim.models.keyedvectors as word2vec
import libunivencoder as uenc
def transformSentence(model,sentence,dim):
    twtknr = TweetTokenizer()
    initialArr = np.zeros(dim)
    series = pd.Series(initialArr)
    leng = 1
    if not sentence:
        sentence=''

    for word in twtknr.tokenize(sentence):
        if word in model:
            leng+=1
            currentSeries = np.array(model[word])
            #print("Current series")
            #print(currentSeries)
            initialArr = np.add(initialArr,currentSeries)
            #print("After adding")
            #print(initialArr)
    return pd.Series(initialArr)#/leng


def getEmbeddings(params):

    X_train = params['X_train']
    X_test = params['X_test']
    Y_train = params['Y_train']
    Y_test = params['Y_test']
    
    if params['emsource']=="fasttext" and (params['emmodel']=="skipgram" or params['emmodel']=="cbow"):
        if params['emmodel']=="skipgram":
            if os.path.isfile('./FTSkipgramModel.bin') == False:
                model = fasttext.skipgram(params['ipFile'], 'FTSkipgramModel',lr=0.1,epoch=100,dim=150)
            else:
                 model =  fasttext.load_model('./FTSkipgramModel.bin')
        elif params['emmodel']== "cbow":
            if os.path.isfile('./FTCBOWModel.bin') == False:
                model = fasttext.cbow(params['ipFile'],'FTCBOWModel',lr=0.1,epoch=100)
            else:
                model = fasttext.load_model('./FTCBOWModel.bin')
        X_train = pd.DataFrame([transformSentence(model,sentence,model.dim) for sentence in X_train])
        X_test = pd.DataFrame([transformSentence(model,sentence,model.dim) for sentence in X_test])
    elif params['emsource'] =="fasttext":
        X_train = pd.DataFrame(fasttext.get_vectors(X_train.tolist()))
        X_test = pd.DataFrame(fasttext.get_vectors(X_test.tolist()))
    elif params['emsource']=="glove":
        X_train = pd.DataFrame(glove.get_vectors(X_train.tolist()))
        X_test = pd.DataFrame(glove.get_vectors(X_test.tolist()))
    elif params['emsource']=='w2v':
        model = word2vec.KeyedVectors.load_word2vec_format('../Google/GoogleNews-vectors-negative300.bin', binary=True)       
        X_train = pd.DataFrame([transformSentence(model,sentence,model.vector_size) for sentence in X_train])
        X_test = pd.DataFrame([transformSentence(model,sentence,model.vector_size) for sentence in X_test])
    elif params['emsource']=='infersent':
        X_train  = pd.DataFrame(infersent.get_vectors(X_train.tolist()) )
        X_test  = pd.DataFrame(infersent.get_vectors(X_test.tolist()) )
    elif params['emsource']=="uenc":
        X_train  = pd.DataFrame(uenc.get_vectors(X_train.tolist()))
        X_test  = pd.DataFrame(uenc.get_vectors(X_test.tolist()))
    print(X_train.head())
    """ X_train_glove = pd.DataFrame(glove.get_vectors(X_train[X_train.columns[0]].tolist()))
    X_test_glove = pd.DataFrame(glove.get_vectors(X_test[X_test.columns[0]].tolist()))
    X_train = pd.concat([X_train_glove,X_train],axis=1)
    X_test = pd.concat([X_test_glove,X_test],axis=1)
    print(X_train.shape)
    print(X_test.shape)
    """
    X_train.fillna(0,inplace=True)
    X_test.fillna(0,inplace=True)
        
    return X_train,Y_train,X_test,Y_test
