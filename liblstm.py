import os
import json
import sys
import xml.etree.ElementTree as ET
from sklearn.model_selection import train_test_split

from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm
import numpy as np
import json
import sys
from sklearn.utils import shuffle
import os
import re
import sys
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.optimizers import SGD
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import sklearn
from keras.preprocessing import sequence, text
from keras.layers import GlobalMaxPooling1D, Conv1D, MaxPooling1D, Flatten, Bidirectional, SpatialDropout1D
from keras.layers.recurrent import LSTM, GRU
from keras.utils import to_categorical
from keras.layers import Convolution1D, MaxPooling1D, merge
#import load_test_data
from tqdm import tqdm
import tensorflow as tf
from tensorflow.python.keras import backend as K


#embeddings_file = '/data/glove.840B/glove.840B.300d.txt'
embeddings_file = '/data/glove.twitter.27B/glove.twitter.27B.200d.txt'
#embeddings_file = './FTSkipgramModel.vec'

arch = 'lstm'

def f1(y_true, y_pred):
    y_pred = K.round(y_pred)
    tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
    # tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
    fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
    fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

    p = tp / (tp + fp + K.epsilon())
    r = tp / (tp + fn + K.epsilon())

    f1 = 2*p*r / (p+r+K.epsilon())
    f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
    return K.mean(f1)

def fit_predict(X_raw_train, y_train, X_raw_test, y_test):
  embeddings_index = {}
  f = open(embeddings_file)
  for line in tqdm(f,total=1193514, desc="#Vectors"):
    values = line.split(" ")
    #print(values)
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
  f.close()
  embedding_size = len(coefs)
  print('Found %s word vectors.' % len(embeddings_index))
  # Input parameters
  max_features = 5000
  max_len = 100

  # Convolution parameters
  filter_length = 3
  nb_filter = 150
  pool_length = 2
  cnn_activation = 'relu'
  border_mode = 'same'
  # RNN parameters
  output_size = 200
  rnn_activation = 'tanh'
  recurrent_activation = 'hard_sigmoid'

  # Compile parameters
  loss = 'binary_crossentropy'
  optimizer = 'rmsprop'

  # Training parameters
  batch_size = 64
  nb_epoch = 8
  validation_split = 0.10
  shuffle = True


  # Build vocabulary & sequences
  tk = text.Tokenizer(num_words=max_features, lower=True, split=" ")
  tk.fit_on_texts(X_raw_train)
  X_train = tk.texts_to_sequences(X_raw_train)
  word_index = tk.word_index
  X_train = sequence.pad_sequences(X_train,maxlen=max_len)

  # create an embedding matrix for the words we have in the dataset
  embedding_matrix = np.zeros((len(word_index) + 1, embedding_size))
  for word, i in tqdm(word_index.items()):
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector

  embedding_layer = Embedding(input_dim = len(word_index)+1,
                              output_dim = embedding_size,
                              weights=[embedding_matrix],
                              input_length=max_len)

  dropout=0.2
  model = Sequential()
  model.add(embedding_layer)
  model.add(Dropout(dropout))
  #model.add(LSTM(output_dim=output_size,activation=rnn_activation,recurrent_activation=recurrent_activation))
  #model.add(Dropout(0.2))
  #model.add(Conv1D(64, 5, activation='relu',input_shape=(None,embedding_size)))
  #model.add(MaxPooling1D(pool_size=4))
  model.add(LSTM(output_dim=output_size,activation=rnn_activation,recurrent_activation=recurrent_activation))
  model.add(Dropout(dropout))
  model.add(Dense(1))
  model.add(Activation('sigmoid'))

  model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy',f1])

  print('LSTM')
  model.fit(X_train, y_train, batch_size=batch_size, epochs=nb_epoch,validation_split=validation_split,shuffle=shuffle)
  X_test_seq = tk.texts_to_sequences(X_raw_test)
  X_test = sequence.pad_sequences(X_test_seq, maxlen=max_len)
  y_pred = model.predict(X_test)
  y_pred = [ int(round(i[0])) for i in y_pred]
  print( confusion_matrix(y_test, y_pred))
  print( classification_report(y_test, y_pred, digits=4))
  return y_pred

if __name__ == "__main__":
  main()
