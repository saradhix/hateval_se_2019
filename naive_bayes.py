import numpy as np
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import MultinomialNB
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from nltk.tokenize import TweetTokenizer
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS
from textstat.textstat import *
import csv
from sklearn.model_selection import cross_val_score
import evaluate_model as em
from sklearn.model_selection import StratifiedKFold

def get_sentiment(sentence):
    sentiment_analyzer = VS()
    sentiment = sentiment_analyzer.polarity_scores(sentence)
    return [sentiment['pos'],sentiment['neg'],sentiment['neu'],sentiment['compound']]

def pos_tagger(sentence):
    tokens=[]
    twtknr = TweetTokenizer()
    for word in twtknr.tokenize(sentence):
        tokens.append(word)
    pos_tag_dict = nltk.pos_tag(tokens)
    pos_tags = [pos_tag_elem[1] for pos_tag_elem in pos_tag_dict]
    pos_tags = " ".join(str(x) for x in pos_tags)
    return pos_tags



def fit_predict(X_train, y_train, X_test, y_test, emr,label):
    """
    tfidf = TfidfVectorizer(min_df=3,max_features=None,
                             strip_accents='unicode',analyzer='word',
                             token_pattern=r'\w{1,}',ngram_range=(1,2),
                             use_idf=1,smooth_idf=1,stop_words='english',
                             )"""

    tfidf = CountVectorizer(min_df=3,max_features=None, 
                             strip_accents='unicode',analyzer='word',
                             token_pattern=r'\w{2,}',
                             ngram_range=(1,2),
                             stop_words='english',
                             binary=True
                             )
    """ 
    #Sentiment analyzer
    pre_train_fn = './preprocess_en_train_taskA_vader.tsv'
    pre_dev_fn = './preprocess_en_dev_taskA_vader.tsv'

    x_train_sen = pd.read_csv(pre_train_fn,sep=",",header=None).values
    x_test_sen = pd.read_csv(pre_dev_fn,sep=",",header=None).values
    print(len(x_train_sen))
    """
    #Vectorizing input
    print("Transforming train")
    X_train = np.array((tfidf.fit_transform(X_train).toarray()))
    print("Transforming test")
    X_test = np.array((tfidf.transform(X_test).toarray()))

    """#Combining features
    X_train = pd.DataFrame(np.concatenate([X_train,x_train_sen],axis=1))
    X_test =  pd.DataFrame(np.concatenate([X_test,x_test_sen],axis=1))
    X_train = (X_train - X_train.min())/(X_train.max()-X_train.min())
    X_test = (X_test - X_test.min())/(X_test.max()-X_test.min())
    X_test.fillna(0,inplace=True)
    """


    clf =  MultinomialNB()
    print("Naive Bayes train")
    #print("#Features=%d #instances=%d" % (X_train.shape[1], X_train.shape[0]))i
    # em.cross_val(np.array(X_train),np.array(y_train),clf)
    scores = cross_val_score(clf,X_train,y_train,cv=5,scoring='f1_macro')
    skf = StratifiedKFold(n_splits=5)
    for train_index, test_index in skf.split(X_train, y_train):
       #print(train_index.dtype,test_index.dtype)
       X_train1, X_test1 = X_train[train_index], X_train[test_index]
       y_train1, y_test1 = y_train[train_index], y_train[test_index]
       clf.fit(X_train1, y_train1)
       y_pred1 = clf.predict(X_test1)
       #print(emr.keys())
       print("Appending",label)
       emr['nb'][label].append(np.equal(y_test1 , y_pred1))
     
    print(scores)
    print(scores.mean())
    clf.fit(X_train, y_train)
    print("NaiveBayes predict")
    y_pred = clf.predict(X_test)
    print( confusion_matrix(y_test, y_pred))
    print( classification_report(y_test, y_pred, digits=4))
    #Dump the model
    #pickle_file = 'tfidf_logreg_model.pickle'
    #pickle.dump(clf, open(pickle_file,"wb"))
    """
    weights=[]
    id2word={v: k for k, v in tfidf.vocabulary_.items()}
    print(clf.coef_.shape)
    print(clf.intercept_)
    for index, weight in enumerate(clf.coef_[0].tolist()):
        weights.append((index, weight))
    print(len(tfidf.vocabulary_))
    top_weights = sorted(weights, key=lambda x:x[1], reverse=True)[:100]
    words_weights = [(id2word[wid], weight) for wid, weight in top_weights]
    print(words_weights)
    top_weights = sorted(weights, key=lambda x:x[1], reverse=True)[-100:]
    words_weights = [(id2word[wid], weight) for wid, weight in top_weights]
    print(words_weights)"""
    return y_pred,emr


def transformSentence(model,sentence):
    twtknr = TweetTokenizer()
    series = pd.Series()
    leng = 0
    for word in twtknr.tokenize(sentence):
        if word in model.words:
            leng+=1
            if len(series) == 0:
                series = pd.Series(model[word])
            else:
                currentSeries = model[word]
                series.add(currentSeries,fill_value=0)            
    return series/leng



def fit_predict_fasttext(params):

    model = fasttext.load_model('model.bin')
    X_train = params['X_train']
    X_test = params['X_test']
    Y_train = params['Y_train']
    Y_test = params['Y_test']

    X_train = pd.DataFrame([transformSentence(model,sentence) for sentence in X_train])
    X_test = pd.DataFrame([transformSentence(model,sentence) for sentence in X_test])
   
    print(X_test.shape)
    print(Y_train.shape)
    """clf =  MultinomialNB()
    print("Naive Bayes train")
    #print("#Features=%d #instances=%d" % (X_train.shape[1], X_train.shape[0]))
    clf.fit(X_train, Y_train)
    print("NaiveBayes predict")
    Y_pred = clf.predict(X_test)
    print( confusion_matrix(Y_test, Y_pred))
    print( classification_report(Y_test, Y_pred, digits=4))
    return Y_pred"""
   
    

