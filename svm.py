import numpy as np
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
#from sklearn.svm import LinearSVC
from sklearn import preprocessing
import pandas as pd
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold

def fit_predict(X_train, y_train, X_test, y_test,emr,label):
    tfidf = TfidfVectorizer(min_df=3,max_features=None, 
                             strip_accents='unicode',analyzer='word',
                             token_pattern=r'\w{1,}',ngram_range=(1,2),
                             use_idf=1,smooth_idf=1,stop_words='english',
                             )
    """print("Transforming train")
    X_train = tfidf.fit_transform(X_train)
    print("Transforming test")
    X_test = tfidf.transform(X_test)
    """
    """X_train = (X_train - X_train.min())/(X_train.max()-X_train.min())
    X_test =  (X_test - X_test.min())/(X_test.max()-X_test.min())
    X_test.fillna(0,inplace=True) 
    X_train.fillna(0,inplace=True)"""
    """selector = SelectKBest(chi2, k=150)
    X_train = selector.fit_transform(X_train, y_train)
    X_test = selector.transform(X_test)
    
    """
    """scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)"""
    X_train = np.array(X_train)
    X_test = np.array(X_test)
    y_train = np.array(y_train)
    y_test = np.array(y_test)
 
    kernels = ['poly','rbf','linear']
    for kernel in [kernels[1]]:
        clf = svm.SVC(kernel=kernel,random_state=0, tol=1e-5)
        """skf = StratifiedKFold(n_splits=5)
        for train_index, test_index in skf.split(X_train, y_train):
           print(train_index.dtype,test_index.dtype)
           X_train1, X_test1 = X_train[train_index], X_train[test_index]
           y_train1, y_test1 = y_train[train_index], y_train[test_index]
           clf.fit(X_train1, y_train1)
           y_pred1 = clf.predict(X_test1)
           print(emr.keys())
           print("Appending",label)
           emr['svm'][label].append(np.equal(y_test1 , y_pred1))
        
        print("SVM train")
        #print("#Features=%d #instances=%d" % (X_train.shape[1], X_train.shape[0]))i
        #scores = cross_val_score(clf,X_train,y_train,cv=5,scoring='f1_macro')
        #print(scores)
        #print(scores.mean())
        """
        clf.fit(X_train, y_train)
        print("SVM predict")
        y_pred = clf.predict(X_test)
        print( confusion_matrix(y_test, y_pred))
        print( classification_report(y_test, y_pred, digits=4))
        #Dump the model
        #pickle_file = 'svm_model.pickle'
        #pickle.dump(clf, open(pickle_file,"wb"))
        weights=[]
        """id2word={v: k for k, v in tfidf.vocabulary_.items()}
        print(clf.coef_.shape)
        print(clf.intercept_)
        for index, weight in enumerate(clf.coef_[0].tolist()):
            weights.append((index, weight))
        print(len(tfidf.vocabulary_))
        top_weights = sorted(weights, key=lambda x:x[1], reverse=True)[:100]
        words_weights = [(id2word[wid], weight) for wid, weight in top_weights]
        print(words_weights)
        top_weights = sorted(weights, key=lambda x:x[1], reverse=True)[-100:]
        words_weights = [(id2word[wid], weight) for wid, weight in top_weights]
        print(words_weights)"""
        return y_pred,emr

