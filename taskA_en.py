import pandas as pd
import tf_idf
import preprocessor as p
from nltk.stem import PorterStemmer as ps
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tokenize import TweetTokenizer
import naive_bayes
import svm
import contractions
import nltk
import random_forest
import xg_boost
nltk.download('punkt')
import argparse
import word_embeddings as we
from sklearn.naive_bayes import MultinomialNB
import os
import logreg
import string
import random_forest
import liblstm as lstm
import libinfersent
import split_hashtags as sh
import re
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
W2V_PATH = '../Google/GoogleNews-vectors-negative300.bin'
import gensim
import numpy as np
import libekphrasis as ek
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS
from textstat.textstat import *
import csv

def stemmer(stemmerObj,sentence):
    twtknr = TweetTokenizer()
    #print(sentence)
    if sentence is None or isinstance(sentence,str)==False:
        return ' '
    sentence = [stemmerObj.stem(word) for word in twtknr.tokenize(sentence)]
    return " ".join(str(x) for x in sentence)


def uniques( sentence ):    
    seen = set()
    twtknr = TweetTokenizer()
    return " ".join( seen.add(i) or i for i in twtknr.tokenize(sentence) if i not in seen )

def replace(word):
    if word.lower() in contractions.contractions:
        return contractions.contractions[word.lower()]
    return word

def replaceContractions(sentence):
    twtknr = TweetTokenizer()
    sentence = [replace(word) for word in twtknr.tokenize(sentence)]
    return " ".join(str(x) for x in sentence)

def removePunc(sentence,translator):
    return  sentence.translate(translator)

def removeNums(sentence,translator):
    return sentence.translate(translator)


def split_on_uppercase(s, keep_contiguous=False):
    """

    Arg:
        s (str): string
        keep_contiguous (bool): flag to indicate we want to 
                                keep contiguous uppercase chars together

    Returns:

    """

    string_length = len(s)
    is_lower_around = (lambda: s[i-1].islower() or 
                       string_length > (i + 1) and s[i + 1].islower())

    start = 0
    parts = []
    for i in range(1, string_length):
        if s[i].isupper() and (not keep_contiguous or is_lower_around()):
            parts.append(s[start: i])
            start = i
    parts.append(s[start:])

    return parts

def maxProbableIndex(wordSplits,model):
    values = np.array([])
    for i in range(len(wordSplits)):
        sim=0
        for j in range(len(wordSplits[i])-1):
            if wordSplits[i][j] in model.vocab and wordSplits[i][j+1] in model.vocab:
                sim += model.similarity(wordSplits[i][j],wordSplits[i][j+1])
        values = np.append(values,[sim/len(wordSplits[i])])
    if len(values)>=2:
        return np.argmax(values)
    else:
        return 0


def splitTag(word,model):
    #print(word)
    #print(word is None)
    #print(word.startswith("#"))

    if word is None or word.startswith("#") == False:
        return word.lower()
    #print(word)
    word = word[1:]
    #wordSplits = split_on_uppercase(word,True)
    #print(wordSplits)
    #if len(wordSplits)>1:
    #    return " ".join(wordSplits)

    
    word = word.lower()
    #print("Goinf to split")
    wordSplits = sh.split_hashtag_to_words_all_possibilities(word)
   
    print(wordSplits)
    index = maxProbableIndex(wordSplits,model)
    print(index)
    if len(wordSplits)!=0:
        return " ".join(wordSplits[index])
    else:
        return word

def removeHashtags(sentence,model):
    #print("Rem hashtags")
    #print(sentence)
    twtknr = TweetTokenizer()
    #print(sentence)
    sentence = [splitTag(word,model) for word in twtknr.tokenize(sentence)]
    print("Done")
    return " ".join(str(x) for x in sentence)

def get_sentiment(sentence,sentiment_analyzer):
    sentiment = sentiment_analyzer.polarity_scores(sentence)
    return ",".join(str(x) for x in [sentiment['pos'],sentiment['neg'],sentiment['neu'],sentiment['compound']])


def preprocess_vader(train,test):
    pre_train_fn = './preprocess_en_train_taskA_vader.tsv'
    pre_dev_fn = './preprocess_en_dev_taskA_vader.tsv'
    if os.path.isfile(pre_train_fn) == True and os.path.isfile(pre_dev_fn) == True:
        return pre_train_fn,pre_dev_fn

    x_train = train['text']
    x_test = test['text']

    p.set_options(p.OPT.URL,p.OPT.MENTION,p.OPT.EMOJI,p.OPT.SMILEY)
    x_train = [p.clean(sentence) for sentence in x_train]
    x_test = [p.clean(sentence) for sentence in x_test]

    x_train = [replaceContractions(sentence) for sentence in x_train]
    x_test = [replaceContractions(sentence) for sentence in x_test]

    translator =  str.maketrans(string.punctuation.replace('#',''),' '*len(string.punctuation.replace('#','')))

    #x_train = [removePunc(sentence,translator) for sentence in x_train]
    #x_test = [removePunc(sentence,translator) for sentence in x_test]

    translator = str.maketrans(string.digits,' '*len(string.digits))
    x_train = [removeNums(sentence,translator) for sentence in x_train]
    x_test  = [removeNums(sentence,translator) for sentence in x_test]

    x_train = [ " ".join(ek.text_processor.pre_process_doc(sentence)) for sentence in x_train]
    x_test = [ " ".join(ek.text_processor.pre_process_doc(sentence)) for sentence in x_test]

    sentiment_analyzer = VS()
    x_train = [get_sentiment(sentence,sentiment_analyzer) for sentence in x_train]
    x_test = [get_sentiment(sentence,sentiment_analyzer) for sentence in x_test]
    
   
    print(x_train[0])
    
    train = pd.DataFrame(x_train)
    test = pd.DataFrame(x_test)

    if os.path.isfile(pre_train_fn) == False:
        f = open(pre_train_fn,"w")
        train.to_csv(f,sep=',',quoting=csv.QUOTE_NONE,quotechar='',escapechar=' ',header=False,index=False)
    if os.path.isfile(pre_dev_fn) == False:
        f = open(pre_dev_fn,"w")
        test.to_csv(f,sep=',',quoting=csv.QUOTE_NONE,quotechar='',escapechar=' ',header=False,index=False)
    print("Completed writing preprocessed files")
    return pre_train_fn,pre_dev_fn

    


def preprocess_pos(train,test):
    pre_train_fn = './preprocess_en_train_taskA_vader.tsv'
    pre_dev_fn = './preprocess_en_dev_taskA_vader.tsv'
    if os.path.isfile(pre_train_fn) == True and os.path.isfile(pre_dev_fn) == True:
        return pre_train_fn,pre_dev_fn

    x_train = train['text']
    x_test = test['text']

    p.set_options(p.OPT.URL,p.OPT.MENTION,p.OPT.EMOJI,p.OPT.SMILEY)
    x_train = [p.clean(sentence) for sentence in x_train]
    x_test = [p.clean(sentence) for sentence in x_test]

    x_train = [replaceContractions(sentence) for sentence in x_train]
    x_test = [replaceContractions(sentence) for sentence in x_test]

    translator =  str.maketrans(string.punctuation.replace('#',''),' '*len(string.punctuation.replace('#','')))

    x_train = [removePunc(sentence,translator) for sentence in x_train]
    x_test = [removePunc(sentence,translator) for sentence in x_test]

    translator = str.maketrans(string.digits,' '*len(string.digits))
    x_train = [removeNums(sentence,translator) for sentence in x_train]
    x_test  = [removeNums(sentence,translator) for sentence in x_test]

    x_train = [ " ".join(ek.text_processor.pre_process_doc(sentence)) for sentence in x_train]
    x_test = [ " ".join(ek.text_processor.pre_process_doc(sentence)) for sentence in x_test]

    sentiment_analyzer = VS()
    x_train = [get_sentiment(sentence,sentiment_analyzer) for sentence in x_train]
    x_test = [get_sentiment(sentence,sentiment_analyzer) for sentence in x_test]


    print(x_train[0])

    train = pd.DataFrame(x_train)
    test = pd.DataFrame(x_test)

    if os.path.isfile(pre_train_fn) == False:
        f = open(pre_train_fn,"w")
        train.to_csv(f,sep=',',quoting=csv.QUOTE_NONE,quotechar='',escapechar=' ',header=False,index=False)
    if os.path.isfile(pre_dev_fn) == False:
        f = open(pre_dev_fn,"w")
        test.to_csv(f,sep=',',quoting=csv.QUOTE_NONE,quotechar='',escapechar=' ',header=False,index=False)
    print("Completed writing preprocessed files")
    return pre_train_fn,pre_dev_fn















 
def preprocess(train,test):

    if preprocess==False:
        return train['text'],test['text']
    
    pre_train_fn = './preprocess_en_train_taskA_gold.tsv'
    pre_dev_fn = './preprocess_en_dev_taskA_gold.tsv'
    if os.path.isfile(pre_train_fn) == True and os.path.isfile(pre_dev_fn) == True:
        return pre_train_fn,pre_dev_fn
    
    #train = pd.concat([train,test],axis=0)        
    x_train = train['text']
    y_train = train['HS']

    x_test = test['text']
    y_test = test['HS']
    print(x_test.head())
    p.set_options(p.OPT.URL,p.OPT.MENTION,p.OPT.EMOJI,p.OPT.SMILEY)
    x_train = [p.clean(sentence) for sentence in x_train]
    x_test = [p.clean(sentence) for sentence in x_test]

    #stemmerObj = ps()

    x_train = [replaceContractions(sentence) for sentence in x_train]
    x_test = [replaceContractions(sentence) for sentence in x_test]

    translator =  str.maketrans(string.punctuation.replace('#',''),' '*len(string.punctuation.replace('#','')))

    x_train = [removePunc(sentence,translator) for sentence in x_train]
    x_test = [removePunc(sentence,translator) for sentence in x_test]

    translator = str.maketrans(string.digits,' '*len(string.digits)) 
    x_train = [removeNums(sentence,translator) for sentence in x_train]
    x_test  = [removeNums(sentence,translator) for sentence in x_test]

    #model = gensim.models.KeyedVectors.load_word2vec_format(W2V_PATH, binary=True) 
    #x_train = [removeHashtags(sentence,model) for sentence in x_train]
    #x_test = [removeHashtags(sentence,model) for sentence in x_test]
    
    x_train = [ " ".join(ek.text_processor.pre_process_doc(sentence)) for sentence in x_train]
    x_test = [ " ".join(ek.text_processor.pre_process_doc(sentence)) for sentence in x_test]
    #print(x_train.head())
    #print(x_test.head())
    #x_train = [ stemmer(stemmerObj,sentence) for sentence in x_train ]
    #x_test = [ stemmer(stemmerObj,sentence) for sentence in x_test]


    #x_train = [uniques(sentence) for sentence in x_train]
    #x_test = [uniques(sentence) for sentence in x_test]
    
    train['text'] = pd.Series(x_train)
    test['text']= pd.Series(x_test)

    if os.path.isfile(pre_train_fn) == False:
        f = open(pre_train_fn,"w")
        train.to_csv(f,sep=',')
    if os.path.isfile(pre_dev_fn) == False:
        f = open(pre_dev_fn,"w")
        test.to_csv(f,sep=',')
    print("Completed writing preprocessed files")
    return pre_train_fn,pre_dev_fn

def EMR(ytruths,ypreds):
    count=0
    ytrue1 = ytruths[0]
    ytrue2 = ytruths[1]
    ytrue3 = ytruths[2]
    ypred1,ypred2,ypred3 = ypreds[0],ypreds[1]

    for i in range(len(ypred1)):
        if ypred1[i]==ytrue1[i] and ypred2[i]==ytrue2[i] and ypred3[i]==ytrue3[i]:
            count = count+1

    print(count)


if __name__=="__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-clf","--classifier", help="echo the classifier you use here",action="store",default="logreg")
    parser.add_argument("-emsrc","--emsource", help="echo the source of embedding you use here",action="store",default="")
    parser.add_argument("-emmodel", "--emmodel",help="echo the embedding you use here",action="store",default="")
    parser.add_argument("-pre","--preprocess",help="To tell if preprocess has to happen to text",action="store", default="F")
    parser.add_argument("-op","--writetofile",help="Writes to the output file",action="store",default="F")
    parser.add_argument("-label","--label",help="Which label to calculate on",action="store",default="all")
    parser.add_argument("-train","--train",help="Train file",action="store",default="")
    parser.add_argument("-test","--test",help="Test file",action="store",default="")
    parser.add_argument("-sep","--seperator",help="Seperator in train and test file",action="store",default="\t")
    args = parser.parse_args()
   
    #trainFile = "./public_development_en_A/train_en.tsv"
    #testFile =  "./public_development_en_A/dev_en.tsv"
    #trainFile = "./preprocess_en_train_taskA.tsv"
    #testFile = "./preprocess_en_dev_taskA.tsv"
    #seperator = ","
    trainFile = args.train
    testFile = args.test
    seperator = args.seperator

    train = pd.read_csv(trainFile,sep=seperator)
    test = pd.read_csv(testFile,sep=seperator)
    


    print("Args preprocess is")
    print(args.preprocess)
    if args.preprocess is not None and args.preprocess=="T":
        print("Preprocessing the dataset")
        trainFile,testFile = preprocess(train,test)
        train = pd.read_csv(trainFile,sep=',')
        test = pd.read_csv(testFile,sep=',')
    if args.preprocess is not None and args.preprocess=="vader":
        preprocess_vader(train,test)

    #train = pd.concat([train,test],axis=0)
    #print(train.head())
    #print(test.head())
    train.fillna('abcxyz',inplace=True)
    test.fillna('abcxyz',inplace=True)
    """ 
    y_pred = pd.Series(lstm.fit_predict(train['text'],train['HS'],test['text'],test['HS']))
    print(y_pred.head())
    print(test.head())
    for i in range(len(y_pred)):
        if y_pred[i]==1 and test['HS'].values[i]==0:
            print(test.values[i])
    exit(0)
    """
    results ={'svm':[],'logreg':[],'nb':[],'rf':[],'xgb':[],'lstm':[],'nblogreg':[]}
    emr = {'svm':{'HS':[],'TR':[],'AG':[]},'logreg':{'HS':[],'TR':[],'AG':[]},'nb':{'HS':[],'TR':[],'AG':[]},'rf':{'HS':[],'TR':[],'AG':[]},'xgb':{'HS':[],'TR':[],'AG':[]},'lstm':{'HS':[],'TR':[],'AG':[]},'nblogreg':{'HS':[],'TR':[],'AG':[]}}
    if args.classifier == "lstm":
        trainFile = './preprocess_en_train_taskA_logreg.tsv'
        testFile = './preprocess_en_dev_taskA_logreg.tsv'
        train = pd.read_csv(trainFile,sep=',')
        test = pd.read_csv(testFile,sep=',')
        #print(train.head())
        #print(test.head())
        train.fillna('abcxyz',inplace=True)
        test.fillna('abcxyz',inplace=True)

        ypred = lstm.fit_predict(train['text'],train['HS'],test['text'],test['HS'])
        results['lstm'].append(ypred)
        ypred = lstm.fit_predict(train['text'],train['TR'],test['text'],test['TR'])
        results['lstm'].append(ypred)
        ypred = lstm.fit_predict(train['text'],train['AG'],test['text'],test['AG'])
        ytrue1,ytrue2,ytrue3 = test['HS'],test['TR'],test['AG']
        results['lstm'].append(ypred)
        for key in results.keys():
            count = 0
            if len(results[key])!=0:
                ypred1,ypred2,ypred3 = results[key][0],results[key][1],results[key][2]
                for i in range(len(ypred1)):
                    if ypred1[i]==ytrue1[i] and ypred2[i]==ytrue2[i] and ypred3[i]==ytrue3[i]:
                        count = count+1
                print(key,count)
        


    params ={'emsource':args.emsource,'emmodel':args.emmodel,'X_train':train['text'],'X_test':test['text'],'Y_train':train['HS'],'Y_test':test['HS'],'ipFile':trainFile}
    X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
    lab = 'HS'
    if args.classifier == "logreg" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = logreg.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['logreg'].append(ypred)
    if args.classifier=="svm" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = svm.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['svm'].append(ypred)
    if args.classifier=="nb":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred,emr = naive_bayes.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['nb'].append(ypred)
    if args.classifier=="rf" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = random_forest.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['rf'].append(ypred)
    if args.classifier=="xgb" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = xg_boost.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['xgb'].append(ypred)

    #Final model
    if args.classifier=="nblogreg":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = naive_bayes.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['nblogreg'].append(ypred)


    """#Prepare for nblogreg
    trainFile = './preprocess_en_train_taskA_logreg.tsv'
    testFile = './preprocess_en_dev_taskA_logreg.tsv'
    train = pd.read_csv(trainFile,sep=',')
    test = pd.read_csv(testFile,sep=',')
    print(train.head()) 
    print(test.head())
    train.fillna('abcxyz',inplace=True)
    test.fillna('abcxyz',inplace=True)
    """
    params ={'emsource':args.emsource,'emmodel':args.emmodel,'X_train':train['text'],'X_test':test['text'],'Y_train':train['TR'],'Y_test':test['TR'],'ipFile':trainFile}
    Y_train = train['TR']
    Y_test = test['TR'] 
    """X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
    """

    #Classify TR
    lab = 'TR'
    if args.classifier == "logreg" or args.classifier=="all":
        ypred = logreg.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['logreg'].append(ypred)
    if args.classifier=="svm" or args.classifier=="all":
        ypred = svm.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['svm'].append(ypred)
    if args.classifier=="nb":
        ypred,emr = naive_bayes.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        for i in range(len(ypred)):
            if results['nb'][0][i]==0:
                ypred[i]=0

        results['nb'].append(ypred)
    if args.classifier=="rf" or args.classifier=="all":
        ypred = random_forest.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['rf'].append(ypred)
    if args.classifier=="xgb" or args.classifier=="all":
        ypred = xg_boost.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['xgb'].append(ypred)

    #Final  model
    if args.classifier=="nblogreg":
        ypred = logreg.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['nblogreg'].append(ypred)



    lab = 'AG'
    params ={'emsource':args.emsource,'emmodel':args.emmodel,'X_train':train['text'],'X_test':test['text'],'Y_train':train['AG'],'Y_test':test['AG'],'ipFile':trainFile}
    Y_train = train['AG']
    Y_test = test['AG']
    if args.classifier == "logreg" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = logreg.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['logreg'].append(ypred)
    if args.classifier=="svm" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = svm.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['svm'].append(ypred)
    if args.classifier=="nb":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred,emr = naive_bayes.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        for i in range(len(ypred)):
            if results['nb'][0][i]==0:
                ypred[i]=0

        results['nb'].append(ypred)
    if args.classifier=="rf" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = random_forest.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['rf'].append(ypred)
    if args.classifier=="xgb" or args.classifier=="all":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = xg_boost.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['xgb'].append(ypred)
   
    #Final model     
    if args.classifier=="nblogreg":
        #X_train,Y_train,X_test,Y_test = we.getEmbeddings(params)
        ypred = logreg.fit_predict(X_train,Y_train,X_test,Y_test,emr,lab)
        results['nblogreg'].append(ypred)

    count=0
    ytrue1 = test['HS']
    ytrue2 = test['TR']
    ytrue3 = test['AG']

    
    for key1 in emr.keys():
        score = 0.0
        #print(key1,emr[key1])
        if len(emr[key1]['HS'])>0:
            for i in range(0,5):
                print(type(emr[key1]['HS']))
                score += ((np.array(emr[key1]['HS'][i])==np.array(emr[key1]['TR'][i]))==np.array(emr[key1]['AG'][i])).sum()
            print(key1,score)

    for key in results.keys():
        count = 0
        print(key)
        if len(results[key])!=0:
            ypred1,ypred2,ypred3 = results[key][0],results[key][1],results[key][2]
            #print( classification_report(ytrue1, ypred1, digits=4))
            #print( classification_report(ytrue2, ypred2, digits=4))
            #print( classification_report(ytrue3, ypred3, digits=4))
            for i in range(len(ypred1)):
                if ypred1[i]==ytrue1[i] and ypred2[i]==ytrue2[i] and ypred3[i]==ytrue3[i]:
                    count = count+1
            print(key,count)
  
    #print(test.head()) 
    test.drop(['text','HS','TR','AG'],inplace=True,axis=1)
    #print(test.head())
    test = pd.concat([test['id'],pd.Series(ypred1),pd.Series(ypred2),pd.Series(ypred3)],axis=1)
    #print(test.head())
    #ddprint(word) 
    if args.writetofile !="F":
        key = args.classifier
        #test.drop(['text','HS','TR','AG'],inplace=True,axis=1)
        ypred1,ypred2,ypred3 = results[key][0],results[key][1],results[key][2]
        print(args.label)
        if args.label =="all":
            test = pd.concat([test['id'],pd.Series(ypred1),pd.Series(ypred2),pd.Series(ypred3)],axis=1)
        if args.label =="HS":
            print("Inside")
            print(pd.Series(ypred1).head())
            test = pd.concat([test['id'],pd.Series(ypred1)],axis=1)
        test.to_csv(args.writetofile,sep="\t",index=False,header=False)

    exit(0)
    #print(stemmed_x_train[10])
