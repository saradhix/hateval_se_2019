import numpy as np
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn import linear_model
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
def fit_predict(X_train, y_train, X_test, y_test, emr,label):


    """tfidf = TfidfVectorizer(min_df=3,max_features=None,
                            strip_accents='unicode',analyzer='word',
                            token_pattern=r'\w{1,}',ngram_range=(1,2),
                            use_idf=1,smooth_idf=1,stop_words='english',
                            )
    X_train = np.array(tfidf.fit_transform(X_train).toarray())
    X_test = np.array(tfidf.transform(X_test).toarray())
    """
    """ 
    clf1 = linear_model.LogisticRegression(class_weight = 'balanced',C=0.5,penalty='l1')
    clf1.fit(X_train, y_train)
    print("Logistic regression predict")
    y_pred = clf1.predict(X_test)
    print( confusion_matrix(y_test, y_pred))
    print( classification_report(y_test, y_pred, digits=4))

















    pre_train_fn = './preprocess_en_train_taskA_vader.tsv'
    pre_dev_fn = './preprocess_en_dev_taskA_vader.tsv'

    x_train_sen = pd.read_csv(pre_train_fn,sep=",",header=None).values
    x_test_sen = pd.read_csv(pre_dev_fn,sep=",",header=None).values
    

    print(X_train.shape)
    print(x_train_sen.shape)
    X_train = pd.DataFrame(np.concatenate([X_train,x_train_sen],axis=1))
    X_test =  pd.DataFrame(np.concatenate([X_test,x_test_sen],axis=1))
    X_test.fillna(0,inplace=True)
    """

    """params = { 
             'C':[0.1,0.3,0.5,0.8,1],'penalty':['l1','l2'],'class_weight':['balanced',None],'solver':['liblinear']
             
                
             }
    clf = GridSearchCV(linear_model.LogisticRegression(), params, cv=5, n_jobs=-1)
    """


    X_train = np.array(X_train)
    X_test = np.array(X_test)
    y_train = np.array(y_train)
    y_test = np.array(y_test) 
    clf = linear_model.LogisticRegression(class_weight = 'balanced',C=0.5,penalty='l2')
    skf = StratifiedKFold(n_splits=5)
    for train_index, test_index in skf.split(X_train, y_train):
       X_train1, X_test1 = X_train[train_index], X_train[test_index]
       y_train1, y_test1 = y_train[train_index], y_train[test_index]
       clf.fit(X_train1, y_train1)
       y_pred1 = clf.predict(X_test1)
       emr['logreg'][label].append(np.equal(y_test1 , y_pred1))

    scores = cross_val_score(clf,X_train,y_train,cv=5,scoring='f1_macro')
    print(scores)
    print(scores.mean())
    print("Logistic regression train")
    #print("#Features=%d #instances=%d" % (X_train.shape[1], X_train.shape[0]))
    clf.fit(X_train, y_train)





    """
    clf2 = linear_model.LogisticRegression(class_weight = 'balanced',C=0.5,penalty='l1')
    clf2.fit(X_train, y_train)
    print("Logistic regression predict")
    y_pred = clf2.predict(X_test)
    print( confusion_matrix(y_test, y_pred))
    print( classification_report(y_test, y_pred, digits=4))
    """


















    print("Logistic regression predict")
    y_pred = clf.predict(X_test)
    print( confusion_matrix(y_test, y_pred))
    print( classification_report(y_test, y_pred, digits=4))
    #Dump the model
    """pickle_file = 'tfidf_logreg_model.pickle'
    pickle.dump(clf, open(pickle_file,"wb"))
    weights=[]
    #id2word={v: k for k, v in tfidf.vocabulary_.items()}
     
    print(clf.coef_.shape)
    print(clf.intercept_)
    for index, weight in enumerate(clf.coef_[0].tolist()):
        weights.append((index, weight))
    #print(len(tfidf.vocabulary_))
    top_weights = sorted(weights, key=lambda x:x[1], reverse=True)[:100]
    words_weights = [(id2word[wid], weight) for wid, weight in top_weights]
    print(words_weights)
    top_weights = sorted(weights, key=lambda x:x[1], reverse=True)[-100:]
    words_weights = [(id2word[wid], weight) for wid, weight in top_weights]
    print(words_weights)"""
    return y_pred,emr

